<%-- 
    Document   : date
    Created on : Jan 26, 2018, 7:36:47 PM
    Author     : Etrio Widodo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import="java.sql.*" %> 
<%@ page import="java.io.*" %> 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Ini Date .JSP</h1>
        <form   method="POST" action="hello.jsp">
            <input type="file" name="file" size="50" />
            <input  name="first_name" type="text" />
            <input  name="midle_name" type="text" />
            <input  name="last_name" type="text" />
            <input type="submit" value="OK">
            
        </form>
        <% 
try {
/* Create string of connection url within specified format with machine name, 
port number and database name. Here machine name id localhost and 
database name is usermaster. */ 
String connectionURL = "jdbc:mysql://localhost:3307/daknf"; 

// declare a connection by using Connection interface 
Connection connection = null; 



// Load JBBC driver "com.mysql.jdbc.Driver" 
Class.forName("com.mysql.jdbc.Driver").newInstance(); 

/* Create a connection by using getConnection() method that takes parameters of 
string type connection url, user name and password to connect to database. */ 
connection = DriverManager.getConnection(connectionURL, "root", "etriowidodo");

// check weather connection is established or not by isClosed() method 
if(!connection.isClosed())
%>
<font size="+3" color="green"></b>
<% 
out.println("Successfully connected to " + "MySQL server using TCP/IP...");
connection.close();
}
catch(Exception ex){
%>
</font>
<font size="+3" color="red"></b>
<%
out.println("Unable to connect to database.");
}
%>
    </body>
</html>
