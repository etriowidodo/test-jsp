package admin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class ConnectionManager {
    private static ConnectionManager instance = null;

    private static final String USERNAME = "root";
    private static final String PASSWORD = "etriowidodo";
    private static final String CONN_STRING = "jdbc:mysql://localhost:3307/daknf";

    private Connection conn = null;

    private ConnectionManager() {
    }

    public static ConnectionManager getInstance() {
        if (instance == null) {
            instance = new ConnectionManager();
        }
        return instance;
    }

    private boolean openConnection() throws ClassNotFoundException {
        try {

            conn = DriverManager.getConnection(CONN_STRING, USERNAME, PASSWORD);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;

    }

    public Connection getConnection() {
        if (conn == null) {
            try {
                if (openConnection()) {
                    return conn;
                } else {
                    return null;
                }
            } catch (ClassNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return conn;
    }

    public void close() {
        try {
            conn.close();
            conn = null;
        } catch (Exception e) {
        }
    }

}