package admin;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.JOptionPane;

public abstract class DatabaseObject {

    private static Connection conn = ConnectionManager.getInstance()
            .getConnection();

    public static ResultSet query(String sql) {
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            return rs;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());    
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static boolean update(String sql){
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            int affected = stmt.executeUpdate();

            if(affected == 0){
                return false;
            }

            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public static int insert(String sql){
        try {
            Statement stmt = conn.createStatement();
            int affected = stmt.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);

            return affected;
        } catch (Exception e) {
            return 0;
        }
    }
}