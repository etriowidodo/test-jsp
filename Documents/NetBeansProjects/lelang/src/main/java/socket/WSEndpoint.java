/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socket;

/**
 *
 * @author Etrio Widodo
 */
import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.jboss.logging.Logger;

@ServerEndpoint("/example")
@Stateless
public class WSEndpoint {
    static ScheduledExecutorService timer = 
       Executors.newSingleThreadScheduledExecutor(); 
	Logger log = Logger.getLogger(this.getClass());
	@Resource
	ManagedExecutorService mes;
	private static Set<Session> allSessions;
	@OnMessage
	public void receiveMessage(String message, Session session) {
		log.info("Received : "+ message + ", session:" + session.getId());
                sendTimeToAll(session,message,session.getId());
	}
	
        DateTimeFormatter timeFormatter =  
        DateTimeFormatter.ofPattern("HH:mm: ss");
	@OnOpen
	public void showTime(Session session) {
        allSessions = session.getOpenSessions();
            // start the scheduler on the very first connection
            // to call sendTimeToAll every second   
            log.info("Open session:" + session.getId());
            if (allSessions.size() == 1)  {
//                timer.scheduleAtFixedRate(
//                        () -> sendTimeToAll(session, ""), 0, 1, TimeUnit.SECONDS);
            }
        }
        
        public void sendTimeToAll(Session session,String message,String sessionid) {
        allSessions = session.getOpenSessions();
                for (Session sess : allSessions) {
                    try {
                        if(sess.getId()!=sessionid)
                        {
                           sess.getBasicRemote().sendText(message); 
                        }
                    } catch (IOException ioe) {
                        System.out.println(ioe.getMessage());
                    }
                }
        }
	
	@OnClose
	public void close(Session session, CloseReason c) {
		log.info("Closing:" + session.getId());
	}
}
