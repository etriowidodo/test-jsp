package com.app.lelang.lelang;





import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class DefaultController {
	


	@RequestMapping("/")
	public String index(Model tahun,HttpSession session) {
            if(session.getAttribute("mySessionAttribute")==null){
                return "login";
            }else{
                return "home";
            }
            
	}
        
        
        @RequestMapping("/helloo")
	public String index1(Model tahun,HttpSession session) {
           session.setAttribute("mySessionAttribute", "someValue");
		return "index";
	}
        
        @RequestMapping("/logout")
	public String logout(Model tahun,HttpSession session) {
            session.removeAttribute("mySessionAttribute");
            return "login";
        }
        
//        @RequestMapping("/login")
//	public String index11(Model tahun,HttpSession session) {
//		return "login";
//	}
        
        @RequestMapping("/home")
	public String home(Model tahun,HttpSession session) {
		return "home";
	}
        
        
        @RequestMapping("/auction-today")
	public String AuctionToday(Model tahun,HttpSession session) {
		return "auction-today";
	}
        
        @RequestMapping("/nego-today")
	public String NegoToday(Model tahun,HttpSession session) {
		return "nego-today";
	}
        
        @RequestMapping("/create-auction")
	public String CreateAuction(Model tahun,HttpSession session) {
		return "create-auction";
	}
       
        
        @RequestMapping("/socket2")
	public String Socket2(Model tahun,HttpSession session) {
		return "socket2";
	}
        
       
    

	
}